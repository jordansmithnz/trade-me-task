//
//  TradeMeListing.swift
//  Trade Me
//
//  Created by Jordan Smith on 19/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import Foundation

struct TradeMeListing {
    
    var title: String
    var listingID: Int
    var photoUrl: String?
    var priceDisplay: String?
    
    init?(with data: [String: Any]) {
        
        // Neccessary properties
        
        guard let title = data["Title"] as? String,
            let listingID = data["ListingId"] as? Int else {
            return nil
        }
        
        self.title = title
        self.listingID = listingID
        
        // Optional properties
        
        photoUrl = data["PictureHref"] as? String
        priceDisplay = data["PriceDisplay"] as? String
        
    }
    
}
