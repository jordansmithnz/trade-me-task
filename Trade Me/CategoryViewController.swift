//
//  CategoryViewController.swift
//  Trade Me
//
//  Created by Jordan Smith on 19/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import UIKit

private let categoryCellIdentifer = "Category Cell"
private let preferredContentWidth = CGFloat(400)

protocol CategoryViewControllerDelegate {
    func categoryViewController(_ viewController: CategoryViewController,
                                didFinishWith category: TradeMeCategory)
}

class CategoryViewController: UITableViewController {

    // MARK: - Setup -
    
    var delegate: CategoryViewControllerDelegate?
    var category: TradeMeCategory? {
        didSet {
            title = category?.name
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // If no category is present by this time, assume this
        // is the root category controller
        
        guard category == nil else { return }
        category = dataProvider.rootCategoryNode
        
        // If still not present, attempt to fetch.
        // The fetch will be coalesced if one is already
        // in motion
        
        guard category == nil else { return }
        dataProvider.updateCategoryTree { [weak self] in
            self?.category = dataProvider.rootCategoryNode
            self?.tableView.reloadData()
            self?.updateParentPopoverSize()
        }
        
    }
    
    
    // MARK: - Layout and Display - 
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateParentPopoverSize()
    }
    
    private func updateParentPopoverSize() {
        tableView.layoutIfNeeded()
        navigationController?.preferredContentSize = CGSize(width:
        preferredContentWidth, height: tableView.contentSize.height)
    }
    
    
    // MARK: - TableView Datasource + Delegate -
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category?.subcategories?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: categoryCellIdentifer, for: indexPath)
        let subcategory = category?.subcategories?[indexPath.row]
        cell.textLabel?.text = subcategory?.name
        cell.accessoryType = subcategory?.isLeafNode == false ? .disclosureIndicator : .none
        return cell
    }
    
    
    // MARK: - Transitions/Segues -
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        guard let selectedIndexPath = tableView.indexPathForSelectedRow else { return true }
        guard let selectedCategory = category?.subcategories?[selectedIndexPath.row] else { return true }

        if selectedCategory.isLeafNode {
            dismiss(animated: true)
            delegate?.categoryViewController(self, didFinishWith: selectedCategory)
            return false
        }
        
        return true
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let subcategoryController = segue.destination as? CategoryViewController else { return }
        guard let selectedIndexPath = tableView.indexPathForSelectedRow else { return }
        subcategoryController.category = category?.subcategories?[selectedIndexPath.row]
        subcategoryController.delegate = delegate
        
    }
    
    @IBAction func dismiss() {
        dismiss(animated: true)
        guard let category = category else { return }
        delegate?.categoryViewController(self, didFinishWith: category)
    }

}

