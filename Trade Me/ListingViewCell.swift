//
//  ListingViewCell.swift
//  Trade Me
//
//  Created by Jordan Smith on 19/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import UIKit

private let cornerRadius = CGFloat(12)
private let shadowRadius = CGFloat(15)
private let shadowOpacity = Float(0.1)
private let highlightAnimationDuration = TimeInterval(0.21)
private let highlightAnimationScale = CGFloat(0.97)

class ListingViewCell: UICollectionViewCell {
    
    // MARK: - Setup -
    
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var imageView: UIImageView?
    @IBOutlet var priceLabel: UILabel?
    
    var listing: TradeMeListing? {
        didSet {
            titleLabel?.text = listing?.title
            imageView?.image = ImageCache.preparedImage(for: listing?.photoUrl)
            priceLabel?.text = listing?.priceDisplay
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        contentView.layer.cornerRadius = cornerRadius
        contentView.layer.masksToBounds = true
        layer.cornerRadius = cornerRadius
        layer.shadowRadius = shadowRadius
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .zero
        layer.shadowOpacity = shadowOpacity
        layer.masksToBounds = false
    }
    
    
    // MARK: - Touch Behaviour - 
    
    override var isHighlighted: Bool {
        didSet {
            
            UIView.animate(withDuration: highlightAnimationDuration, delay:
                0, options: [.beginFromCurrentState, .curveEaseOut], animations: { [weak self] in
                
                    let scale = self?.isHighlighted == true ? highlightAnimationScale : 1
                    self?.transform = CGAffineTransform(scaleX: scale, y: scale)
                    
            }, completion: nil)
            
        }
    }
    
}
