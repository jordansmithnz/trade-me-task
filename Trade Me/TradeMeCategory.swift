//
//  TradeMeCategory.swift
//  Trade Me
//
//  Created by Jordan Smith on 19/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import Foundation

private let categoryParsingQueue = DispatchQueue(label:
    "Category Parse Queue", qos: .default, attributes: .concurrent)

// Note: although this is a model type, where a struct type
// would make more sense, the concurrent parsing model
// requires captured mutations of self, which structs
// do not support - so a class it is.

class TradeMeCategory: Equatable {
    
    // MARK: - Setup -
    
    let number: String
    let name: String
    var subcategories: [TradeMeCategory]?
    var parentCategory: TradeMeCategory?
    
    init?(with data: [String: Any], preferredName: String? = nil,
          parent: TradeMeCategory? = nil, andThen: @escaping ()->()) {
     
        // Neccessary properties
        
        guard let name = data["Name"] as? String,
            let number = data["Number"] as? String else {
            return nil
        }
        
        self.name = preferredName ?? name
        self.number = number
        
        // Subcategories
        
        if let subcategorydData = data["Subcategories"] as? [[String: Any]] {
            concurrentlyParse(subcategoryData: subcategorydData, andThen: andThen)
        } else {
            subcategories = nil
            andThen()
        }
        
        // Parent
        
        self.parentCategory = parent
        
    }
    
    
    // MARK: - Convenience Properties -
    
    var isLeafNode: Bool {
        return subcategories?.isEmpty != false
    }
    
    
    // MARK: - Subcateogry Parsing - 
    
    private func concurrentlyParse(subcategoryData: [[String: Any]], andThen: @escaping ()->()) {
        
        // Perhaps concurrent parsing isn't neccessary, but it does 
        // allow for larger category trees in the future, and would make 
        // a more complex parsing routing possible (e.g. images for each category)
        
        var subcategories = [(index: Int, category: TradeMeCategory)]()
        let dispatchGroup = DispatchGroup()

        // Iterate over each category item
        
        for (index, categoryData) in subcategoryData.enumerated() {
            
            // Concurrently perform each operation
            
            dispatchGroup.enter()
            
            categoryParsingQueue.async {
                
                guard let category = TradeMeCategory(with:
                    categoryData, parent: self, andThen: {
                    dispatchGroup.leave()
                }) else { return }
                
                // Concurrent array access safety
                
                objc_sync_enter(self)
                subcategories.append((index: index, category: category))
                objc_sync_exit(self)
                
            }
            
        }
        
        // After all categories have been parsed, sort,
        // populate and run completion
        
        dispatchGroup.notify(queue: categoryParsingQueue) {
            
            let sortedCategories = subcategories.sorted { one, another in
                one.index < another.index
            }
            
            self.subcategories = sortedCategories.map { (index, category) in category }
            andThen()
            
        }
        
    }
    
    
    // MARK: - Equatable Conformance - 
    
    static func ==(category: TradeMeCategory, another: TradeMeCategory) -> Bool {
        return category.number == another.number
    }
    
}
