//
//  ImageCache.swift
//  Trade Me
//
//  Created by Jordan Smith on 20/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import UIKit

class ImageCache {
    
    // This file implements image caching in a style suitable
    // to how the listing view controller populates items - it
    // doesn't display until the image is loaded. There is much
    // room for further extension here, but for brevity only
    // simple memory caching is implemented. In particular, 
    // on disk caching, with fast resizing (to ensure no
    // anti-aliasing artifacts are present) could be added,
    // probably via the ImageIO framework which does both of these
    // things very well.
    
    
    // MARK: - Setup -
    
    private static var imageUrlSession = URLSession(configuration: .default)
    private static var memoryCache = [String: UIImage]()
    
    init() {
        fatalError("ImageCache should never be instantiated")
    }
    
    
    // MARK: - Caching Implementation -
    
    static func prepareRemoteImage(_ location: String?, andThen: @escaping ()->()) {
        
        // Prepare to fetch. Ensure the image doesn't already exist
        
        guard let location = location,
            let imageUrl = URL(string: location) else {
            andThen()
            return
        }
        
        guard memoryCache[location] == nil else {
            andThen()
            return
        }
        
        let completePreparation = {
            DispatchQueue.main.async { andThen() }
        }
        
        // For the current use case, coalescing isn't necessary,
        // but a full implementation should probably coalesce.
        
        let dataTask = imageUrlSession.dataTask(with: imageUrl) { data, response, error in
            
            guard let data = data, let image = UIImage(data: data) else {
                print(error?.localizedDescription ??
                    response?.description ?? "Unknown error retrieving image")
                completePreparation()
                return
            }
            
            memoryCache[location] = image
            completePreparation()
            
        }
        
        dataTask.resume()
        
    }
    
    static func preparedImage(for location: String?) -> UIImage? {
        guard let location = location else { return nil }
        return memoryCache[location]
    }
    
    static func purgeMemoryCache() {
        memoryCache.removeAll()
    }
    
}
