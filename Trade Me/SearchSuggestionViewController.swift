//
//  SearchSuggestionViewController.swift
//  Trade Me
//
//  Created by Jordan Smith on 31/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import UIKit

private let suggestionCellIdentifier = "Search Suggestion Cell"

protocol SearchSuggestionDelegate {
    func handleSearchSuggestion(_ suggestion: TradeMeSearchSuggestion)
}

class SearchSuggestionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Setup -
    
    var delegate: SearchSuggestionDelegate?
    @IBOutlet var tableView: UITableView?
    private var searchSuggestions: [TradeMeSearchSuggestion]?
    
    
    // MARK: - Suggestion Updates -
    
    func updateSearchSuggestions(to suggestions: [TradeMeSearchSuggestion]?, animated: Bool) {
        searchSuggestions = suggestions
        tableView?.reloadData()
    }
    
    // MARK: - TableView Datasource + Delegate - 
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchSuggestions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: suggestionCellIdentifier, for: indexPath)
        cell.textLabel?.text = searchSuggestions?[indexPath.row].searchTerm
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let suggestions = searchSuggestions else { return }
        delegate?.handleSearchSuggestion(suggestions[indexPath.row])
    }
    
}
