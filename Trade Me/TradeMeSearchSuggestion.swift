//
//  TradeMeSuggestion.swift
//  Trade Me
//
//  Created by Jordan Smith on 31/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import Foundation

struct TradeMeSearchSuggestion {
    
    let searchTerm: String
    
    init?(with data: [String: Any]) {
        guard let searchTerm = data["SearchTerm"] as? String else { return nil }
        self.searchTerm = searchTerm
    }
    
}
