//
//  SearchTextField.swift
//  Trade Me
//
//  Created by Jordan Smith on 19/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import UIKit

private let textFieldHeight = CGFloat(40)
private let cornerRadius = CGFloat(7)
private let textFieldBackground = UIColor(white: 0, alpha: 0.075)

class SearchTextField: UITextField {
    
    // MARK: - Setup -
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        self.backgroundColor = textFieldBackground
        clearButtonMode = .always
        let searchIcon = #imageLiteral(resourceName: "Search Icon").withRenderingMode(.alwaysTemplate)
        let searchIconView = UIImageView(image: searchIcon)
        searchIconView.contentMode = .center
        searchIconView.tintColor = .lightGray
        leftView = searchIconView
        leftViewMode = .always
    }

    
    // MARK: - Layout and Display - 
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UILayoutFittingExpandedSize.width, height: textFieldHeight)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 0, width: bounds.height, height: bounds.height)
    }
    
    override func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.width - bounds.height, y: 0,
                      width: bounds.height, height: bounds.height)
    }
    
    
}
