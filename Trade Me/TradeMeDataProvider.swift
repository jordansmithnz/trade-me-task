//
//  TradeMeDataProvider.swift
//  Trade Me
//
//  Created by Jordan Smith on 19/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import Foundation

private let searchAPIUrl = "https://api.tmsandbox.co.nz/v1/Search/General.json?photo_size=FullSize&rows=20"
private let categoryAPIUrl = "https://api.tmsandbox.co.nz/v1/Categories/0.json"
private let searchSuggestionAPIUrl = "https://api.trademe.co.nz/v1/Search/Suggestions.json"
private let rootCategoryName = "All Categories"
private let searchSuggestionDataKey = "AutoSuggestions"

class TradeMeDataProvider {
    
    // MARK: - Setup -

    var rootCategoryNode: TradeMeCategory?
    private let urlSession: URLSession
    private var coalescedCategoryUpdateHandlers = [()->()]()
    
    init(consumerKey: String, consumerSecret: String) {
        
        // Note - the docs specify that we shouldn't use the 'Authorization' 
        // header value in the call below, and to use the delegate function instead.
        // The delegate function doesn't support OAuth, and the reason
        // the docs specify this is because the delegate could stomp on
        // the set value we use here. Without a delegate this works fine,
        // just take care not to override it later via delegate usage.
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = ["Authorization" :
            TradeMeDataProvider.authorizationHeader(withKey: consumerKey, secret: consumerSecret)]
        urlSession = URLSession(configuration: sessionConfiguration)
                
    }
    
    private static func authorizationHeader(withKey key: String, secret: String) -> String {
        
        let consumerKeyString = "oauth_consumer_key=\"\(key)\""
        let consumerSecretString = "oauth_signature=\"\(secret)&\""
        let signatureMethodString = "oauth_signature_method=\"PLAINTEXT\""
        let headerValue = "OAuth " + [consumerKeyString, signatureMethodString,
            consumerSecretString].joined(separator: ",")
        return headerValue
        
    }
    
    
    // MARK: - Data Requests - 
    
    func updateCategoryTree(andThen: (()->())? = nil) {
        
        // Fetch all categories. There are quite a few, but
        // it takes almost no time, and saves the user waiting on requests
        // as they drill through the category tree. A more comprehensive
        // implementation would perhaps persist the tree, and update it
        // as the user navigates throughout (with changes animated
        // into place by the likes of a fetched results controller - although updates
        // seem infrequent, so for the most part no change would be seen). 
        // For now, keep it simple.
        
        guard let fetchUrl = URL(string: categoryAPIUrl) else {
            andThen?()
            return
        }
        
        // Coalesce fetches. If the first fetch failed, a second fetch
        // may be neccessary (e.g. when showing )
        
        defer { coalescedCategoryUpdateHandlers.append { andThen?() } }
        guard coalescedCategoryUpdateHandlers.count == 0 else { return }
        
        // Completion handler for convenience
        
        let completeUpdate = {
            DispatchQueue.main.async {
                
                for updateHandler in self.coalescedCategoryUpdateHandlers {
                    updateHandler()
                }
                
                self.coalescedCategoryUpdateHandlers.removeAll()
                
            }
        }
        
        // Fetch and parse the data
        
        let dataTask = urlSession.dataTask(with: fetchUrl) { (data, response, error) in
            
            switch self.parseResponse(with: data, response, error: error) {
                
            case .success(let responseData):
                self.rootCategoryNode = TradeMeCategory(with:
                    responseData, preferredName: rootCategoryName) {
                    completeUpdate()
                }
                
            case .error(let errorMessage):
                print("Category update failed: \(errorMessage)")
                completeUpdate()
            }
            
        }
        
        dataTask.resume()
        
    }
    
    func retrieveListings(withSearchTerm searchTerm: String?, in category:
        TradeMeCategory? = nil, andThen: @escaping ([TradeMeListing]?, String?)->()) {
        
        // Create the search url
        
        var listingFetchUrl = searchAPIUrl
        
        if let category = category {
            listingFetchUrl += "&category=\(category.number)"
        }
        
        if let searchTerm = searchTerm {
            listingFetchUrl += "&search_string=\(searchTerm)"
        }
        
        guard let fetchUrl = URL(string: listingFetchUrl) else {
            andThen(nil, nil)
            return
        }
        
        // Setup up a main thread completion handler
        
        let completion = { (listings: [TradeMeListing]?, error: String?) in
            DispatchQueue.main.async {
                andThen(listings, error)
            }
        }
        
        // Fetch and parse the data
        
        let dataTask = urlSession.dataTask(with: fetchUrl) { (data, response, error) in
            
            switch self.parseResponse(with: data, response, error: error) {
                
            case .success(let responseData):
                guard let listingData = responseData["List"] as? [[String: AnyObject]] else {
                    completion(nil, "Unrecognized response format")
                    return
                }
                
                let listings = listingData.flatMap { data in TradeMeListing(with: data) }
                completion(listings, nil)
                
            case .error(let errorMessage):
                completion(nil, errorMessage)
                
            }
            
        }

        dataTask.resume()
        
    }
    
    func retrieveSearchSuggestions(forCategory category: TradeMeCategory?,
                                   searchTerm: String, andThen: @escaping ([TradeMeSearchSuggestion])->()) {
        
        // Create the search suggestion Url
        
        var urlString = searchSuggestionAPIUrl + "?search_string=\(searchTerm)"
        
        if let category = category {
            urlString += "&category_id=\(category.number)"
        }
        
        guard let requestUrl = URL(string: urlString) else {
            andThen([])
            return
        }
        
        // Set up a main thread completion handler 
        
        let completion = { (suggestions: [TradeMeSearchSuggestion]?) in
            DispatchQueue.main.async {
                andThen(suggestions ?? [])
            }
        }
        
        // DEBUG ONLY //
        
        andThen([TradeMeSearchSuggestion(with: ["SearchTerm": "Test"])!])
        return
        
        // Fetch and parse the data
        
        let dataTask = urlSession.dataTask(with: requestUrl) { data, response, error in
            
            switch self.parseResponse(with: data, response, error: error) {
            case .success(let responseData):
                
                guard let suggestionData = responseData[searchSuggestionDataKey] as? [[String: Any]] else {
                    completion(nil)
                    return
                }
                
                completion(suggestionData.flatMap { data in
                    TradeMeSearchSuggestion(with: data) })
                
            case .error(_):
                completion(nil)
            }
            
            return
            
        }
        
        dataTask.resume()
        
    }
    
    
    // MARK: - Request Utilities -
    
    private enum Response {
        case success([String : Any])
        case error(String)
    }
    
    private func parseResponse(with data: Data?, _ response: URLResponse?, error: Error?) -> Response {
        
        // Turn the trio of optionals into a straight 
        // forward success or failure, with associated data:
        
        if let error = error {
            return .error(error.localizedDescription)
        }
        
        guard let data = data else {
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {
                return .error("Unknown error retrieving listings")
            }
            return .error(HTTPURLResponse.localizedString(forStatusCode: statusCode))
        }
        
        do {
            let response = try JSONSerialization.jsonObject(with: data, options: [])
            guard let dictionaryResponse = response as? [String: Any] else {
                return .error("Unexpected server response recieved")
            }
            return .success(dictionaryResponse)
        } catch {
            return .error(error.localizedDescription)
        }
        
    }
    
}
