//
//  ListingsViewController.swift
//  Trade Me
//
//  Created by Jordan Smith on 19/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import UIKit

private let listingCellIdentifier = "Listing Cell"
private let categoryControllerIdentifier = "Category Controller"
private let searchSuggestionControllerIdentifier = "Search Suggestion Controller"
private let cellHeightRatio = CGFloat(0.9)
private let goldenRatio = CGFloat(1.6180339887498948482)
private let minimumInsetSize = CGFloat(18)
private let maximumContentWidth = CGFloat(720)
private let stretchedTitleScale = CGFloat(1.06)
private let stretchTravelDistance = CGFloat(120)
private let cancelAnimationDuration = TimeInterval(0.5)
private let cancelAnimationDamping = CGFloat(0.75)

class ListingsViewController: UIViewController, UICollectionViewDelegateFlowLayout,
    UICollectionViewDataSource, CategoryViewControllerDelegate, UITextFieldDelegate, SearchSuggestionDelegate {
    
    // MARK: - Setup - 
    
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var collectionView: StatefulCollectionView?
    @IBOutlet var flowLayout: UICollectionViewFlowLayout?
    @IBOutlet var leadingHeaderConstraint: NSLayoutConstraint?
    @IBOutlet var trailingHeaderConstraint: NSLayoutConstraint?
    @IBOutlet var topHeaderConstraint: NSLayoutConstraint?
    @IBOutlet var searchTextField: UITextField?
    @IBOutlet var headerView: UIView?
    @IBOutlet var searchCancelView: UIView?
    
    private var searchSuggestionViewController: SearchSuggestionViewController?
    private var populationIdentifier: String?
    private var defaultVerticalHeaderOffset = CGFloat(0)
    var listings = [TradeMeListing]()
    
    private var currentCategory: TradeMeCategory? {
        didSet {
            let rootNode = dataProvider.rootCategoryNode
            guard currentCategory ?? rootNode != oldValue ?? rootNode else { return }
            titleLabel?.text = currentCategory?.name
            updateContentMargins()
            refreshListings()
        }
    }
    
    private var currentSearchTerm: String? {
        didSet {
            guard (oldValue ?? "") != (currentSearchTerm ?? "") else { return }
            refreshListings()
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        defaultVerticalHeaderOffset = topHeaderConstraint?.constant ?? 0
        updateContentMargins()
        refreshListings()
        searchCancelView?.isHidden = true
        searchSuggestionViewController = storyboard?.instantiateViewController(withIdentifier: searchSuggestionControllerIdentifier) as? SearchSuggestionViewController
        searchSuggestionViewController?.delegate = self
        searchTextField?.addTarget(self, action: #selector(searchFieldDidChangeValue), for: .editingChanged)
        
    }
    
    
    // MARK: - Content Retrieval - 
    
    private func refreshListings() {
        
        listings.removeAll()
        populationIdentifier = UUID().uuidString
        collectionView?.reloadData()
        collectionView?.displayState = .loading
        
        dataProvider.retrieveListings(withSearchTerm:
            currentSearchTerm, in: currentCategory) { [weak self, populationIdentifier] listings, error in
                
                // Display an error if needed
                
                if let errorMessage = error {
                    self?.collectionView?.displayState =
                        .empty(title: "Error", message: errorMessage, image: #imageLiteral(resourceName: "Error Icon"))
                    return
                }
                
                guard let listings = listings, listings.count > 0 else {
                    
                    let category = self?.currentCategory ?? dataProvider.rootCategoryNode
                    let categoryName = category?.name.lowercased() ?? "all categories"
                    var noResultsMessage = "No results found for \(categoryName)"
                    
                    if let searchTerm = self?.currentSearchTerm,
                        searchTerm.characters.count > 0 {
                        noResultsMessage = "No results found for '\(searchTerm)' in \(categoryName)"
                    }
                    
                    self?.collectionView?.displayState = .empty(title:
                        "No Results", message: noResultsMessage, image: #imageLiteral(resourceName: "No Results Icon"))
                    return
                    
                }
                
                // The population identifier ensures we don't do
                // anything if listings have been refreshed since
                // the initial request. The capture list allows
                // us to compare the current value with the 
                // capture time value.
                
                guard populationIdentifier == self?.populationIdentifier else { return }
                self?.populate(listings: listings)
                
        }
        
    }
    
    private func populate(listings: [TradeMeListing]) {
        
        // Don't show cells until the image is ready.
        // Not neccessarily the normal route to take, 
        // but with large, image filled cells it may
        // look a little odd to display these without 
        // an image at first. Use dispatch groups to 
        // enforce this, where each one is triggered
        // by the previous, so they appear in order
        // (so that cells don't jump as the user tries
        // to tap them)
        
        var previousItemDispatchGroup = DispatchGroup()
                
        for (index, listing) in listings.enumerated() {
            
            let thisItemDispatchGroup = DispatchGroup()
            thisItemDispatchGroup.enter()
            
            ImageCache.prepareRemoteImage(listing.photoUrl) { [weak self,
                thisItemDispatchGroup, previousItemDispatchGroup, populationIdentifier] in
                
                // Do the work when the previous item has completed
                
                previousItemDispatchGroup.notify(queue: .main, execute: {
                    
                    defer { thisItemDispatchGroup.leave() }
                    guard populationIdentifier == self?.populationIdentifier else { return }
                    
                    // Although each change should be self contained, the collection
                    // view insert seems to behave a little funny when not run as
                    // a batch operation. This could be called a lot of times
                    // simulteously, so I wouldn't be surprised if there is some
                    // sort of collection view bug casuing items not to be recalculated
                    // items after each insert correctly. Running this as a batch update
                    // seems to resolve the issue.
                    
                    self?.collectionView?.performBatchUpdates({
                        
                        self?.listings.append(listing)
                        let indexPath = IndexPath(item: (self?.listings.count ?? 1) - 1, section: 0)
                        self?.collectionView?.insertItems(at: [indexPath])
                        
                    }, completion: nil)
                    
                })
                
            }
            
            previousItemDispatchGroup = thisItemDispatchGroup
            
        }
        
    }
    
    
    // MARK: - Layout + Display - 
    
    private func updateContentMargins() {
        
        guard let flowLayout = flowLayout else { return }
        
        // Layout any recent constraint changes first
        
        view.layoutIfNeeded()
        
        // Now proceed with updated view sizes
        
        let contentWidth = min(view.bounds.width - 2 * minimumInsetSize, maximumContentWidth)
        let horizontalInsetSize = (view.bounds.width - contentWidth) / 2
        leadingHeaderConstraint?.constant = horizontalInsetSize
        trailingHeaderConstraint?.constant = horizontalInsetSize
        collectionView?.contentInset.top = headerView?.bounds.height ?? 0
        flowLayout.sectionInset = UIEdgeInsets(top: flowLayout.minimumInteritemSpacing, left: horizontalInsetSize,
                                               bottom: flowLayout.minimumInteritemSpacing, right: horizontalInsetSize)
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        // Update layout during resize, e.g iPad rotation
        
        coordinator.animate(alongsideTransition: { _ in
            
            self.updateContentMargins()
            self.collectionView?.performBatchUpdates({ 
                self.flowLayout?.invalidateLayout()
            }, completion: nil)
            
        }, completion: nil)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // Bouncy header, a la. iOS 11
        
        topHeaderConstraint?.constant = max(defaultVerticalHeaderOffset - scrollView.contentOffset.y - scrollView.contentInset.top, defaultVerticalHeaderOffset)
        let verticalOffset = -scrollView.contentInset.top - scrollView.contentOffset.y
        let travelFraction = max(min(verticalOffset / stretchTravelDistance, 1), 0)
        let titleScale = 1 + (stretchedTitleScale - 1) * travelFraction
        let scaleTransform = CGAffineTransform(scaleX: titleScale, y: titleScale)
        let translationTransform = CGAffineTransform(translationX:
            (titleLabel?.bounds.midX ?? 0) * (titleScale - 1), y: 0)
        titleLabel?.transform = scaleTransform.concatenating(translationTransform)
        titleLabel?.layer.rasterizationScale = titleScale == 1 ? 1 : 2 // Prevent pixellation
        
        // Update the view for the new constraint
        
        view.layoutIfNeeded()
        
        // Use the updated view height for scroll insets
        
        collectionView?.scrollIndicatorInsets.top = (headerView?.bounds.height ?? 0)
        
    }
    
    
    // MARK: - Collection View Datasource + Delegate -
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: listingCellIdentifier, for: indexPath)
        (cell as? ListingViewCell)?.listing = listings[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let flowLayout = flowLayout else { return .zero }
        let availableWidth = collectionView.bounds.width -
            flowLayout.sectionInset.left - flowLayout.sectionInset.right
        
        // For compact size classes, display a full width cell, and determine the height via ratio

        guard traitCollection.horizontalSizeClass == .regular else {
            return CGSize(width: availableWidth, height: availableWidth * cellHeightRatio)
        }
        
        // For regular width, populate cells in a small large, large small repeating pattern.
        // Use the golden ratio to determine widths, and the previous ratio for height
        
        let totalCellWidth = availableWidth - flowLayout.minimumInteritemSpacing
        let smallCellWidth = totalCellWidth / (1 + goldenRatio)
        let largeCellWidth = smallCellWidth * goldenRatio
        let cellHeight = smallCellWidth * cellHeightRatio
        
        if indexPath.row % 4 == 0 || indexPath.row % 4 == 3 {
            return CGSize(width: smallCellWidth, height: cellHeight)
        } else {
            return CGSize(width: largeCellWidth, height: cellHeight)
        }
        
    }
    
    
    // MARK: - Transitions + Segue Presentation - 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        var pertinentViewController: UIViewController? = segue.destination
        
        // Handle nested view controllers. A future split view controller could
        // nest navigation controllers, while a 3D touch interaction might have none,
        // etc...
        
        while pertinentViewController is UINavigationController {
            pertinentViewController = (pertinentViewController as? UINavigationController)?.topViewController
        }
        
        if let rootCategoryController = pertinentViewController as? CategoryViewController {
            
            // Start by instantiating with the root category
            
            rootCategoryController.delegate = self
            rootCategoryController.category = dataProvider.rootCategoryNode
            let categoryNavigationController = rootCategoryController.navigationController
            let popoverController = categoryNavigationController?.popoverPresentationController
            popoverController?.permittedArrowDirections = .up
            
            // Position a possible popover presentation controller correctly
            
            if let categoryButton = sender as? UIView {
                popoverController?.sourceRect = categoryButton.bounds
                popoverController?.sourceView = categoryButton
            }
            
            // Gather the parent category stack from the 
            // currently selected category
            
            var categoryStack = [TradeMeCategory]()
            var traversalCategory = currentCategory
            
            while let category = traversalCategory {
                
                traversalCategory = category.parentCategory

                // Don't insert leaf node categories, otherwise
                // the user would be presented a blank page.
                // A better implementation might be to show the
                // page with an explanation + image + back button, 
                // but for the sake of brevity...
                
                guard category.isLeafNode == false else { continue }
                categoryStack.insert(category, at: 0)
                
            }
            
            // The root controller has already been initialized via segue,
            // so we only need to worry about categories after the first
            
            let firstChildIndex = min(1, categoryStack.count)
            
            // Restore the navigation state so the user is presented
            // with the current category:
            
            for category in categoryStack[firstChildIndex ..< categoryStack.count] {
                
                guard let childController = storyboard?.instantiateViewController(withIdentifier:
                    categoryControllerIdentifier) as? CategoryViewController else { return }
                childController.category = category
                childController.delegate = self
                categoryNavigationController?.pushViewController(childController, animated: false)
                
            }
            
        }
        
    }
    
    @IBAction func cancelSearch() {
        searchTextField?.text = currentSearchTerm
        searchTextField?.resignFirstResponder()
    }
    
    private func updateSearchCancelView(hidden: Bool) {
        
        guard searchCancelView?.isHidden != hidden else { return }
        
        // Set up for a fade in on appearance
        
        if hidden == false { searchCancelView?.alpha = 0 }
        
        // Show the cancel button with a lsubtle spring effect
        
        UIView.animate(withDuration: cancelAnimationDuration, delay: 0,
                       usingSpringWithDamping: cancelAnimationDamping,
                       initialSpringVelocity: 0, options: .beginFromCurrentState,
                       animations: { [weak self] in
                        
            self?.searchCancelView?.isHidden = hidden
            self?.searchCancelView?.alpha = hidden ? 0 : 1
                        
        }, completion: nil)
        
    }
    
    
    // MARK: - Category View Controller Delegate - 
    
    func categoryViewController(_ viewController: CategoryViewController,
                                didFinishWith category: TradeMeCategory) {
        currentCategory = category
    }
    
    
    // MARK: - Text Field Delegate - 
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentSearchTerm = textField.text
        updateSearchCancelView(hidden: true)
        setSearchSuggestionsHidden(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateSearchCancelView(hidden: false)
    }
    
    
    // MARK: - Search Suggestion Delegate - 
    
    func handleSearchSuggestion(_ suggestion: TradeMeSearchSuggestion) {
        searchTextField?.text = suggestion.searchTerm
        view.endEditing(true)
    }
    
    
    // MARK: - Search Suggestion Display + Hiding -
    
    @objc func searchFieldDidChangeValue() {
        
        guard let searchText = searchTextField?.text else {
            setSearchSuggestionsHidden(true)
            return
        }
        
        dataProvider.retrieveSearchSuggestions(forCategory:
        currentCategory, searchTerm: searchText) { [weak self] suggestions in
            
            // Only continue if the user hasn't typed more in the meantime
            
            guard searchText == self?.searchTextField?.text else { return }
            
            // Dismiss or present the controller if needed:
            
            guard suggestions.count > 0 else {
                self?.setSearchSuggestionsHidden(true)
                return
            }
            
            if self?.presentedViewController != self?.searchSuggestionViewController {
                self?.setSearchSuggestionsHidden(false)
            }
            
            // Update the results
            
            self?.searchSuggestionViewController?.updateSearchSuggestions(to: suggestions, animated: true)
            
        }
        
    }
    
    private func setSearchSuggestionsHidden(_ hidden: Bool) {
        
        // 
        
    }
    
}
