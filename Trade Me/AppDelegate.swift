//
//  AppDelegate.swift
//  Trade Me
//
//  Created by Jordan Smith on 19/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import UIKit

// App wide data provider
let dataProvider = TradeMeDataProvider(consumerKey: "A1AC63F0332A131A78FAC304D007E7D1",
                                       consumerSecret: "EC7F18B17A062962C6930A8AE88B16C7")

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        dataProvider.updateCategoryTree()
        return true
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        ImageCache.purgeMemoryCache()
    }
    
}

