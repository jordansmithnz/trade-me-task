//
//  HorizontalSeparatorView.swift
//  Trade Me
//
//  Created by Jordan Smith on 20/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import UIKit

private let separatorThickness = CGFloat(0.5)

@IBDesignable
class HorizontalSeparatorView: UIView {
    
    // MARK: - Setup -
    
    @IBInspectable var color: UIColor = .init(white: 0, alpha: 0.18) {
        didSet {
            setNeedsDisplay()
        }
    }
    
    
    // MARK: - Layout and Display -
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        guard let context = UIGraphicsGetCurrentContext() else { return }
        color.setStroke()
        
        // Draw the stroke along the bottom of the view, unless
        // .top is specified as the content mode
        
        let verticalOffset = contentMode == .top ?
            separatorThickness / 2 : bounds.height - separatorThickness / 2
        context.move(to: CGPoint(x: rect.minY, y: verticalOffset))
        context.addLine(to: CGPoint(x: rect.maxX, y: verticalOffset))
        context.strokePath()
        
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UILayoutFittingExpandedSize.width, height: separatorThickness)
    }
    
}
