//
//  StatefulCollectionView.swift
//  Trade Me
//
//  Created by Jordan Smith on 20/08/17.
//  Copyright © 2017 Jordan Smith. All rights reserved.
//

import UIKit

// This collection view can display an empty
// state during a loading or empty state.
// It automatically removes the displayed view
// when it becomes populated.

class StatefulCollectionView: UICollectionView {
    
    // MARK: - Setup -
    
    enum EmptyDisplayState {
        case loading
        case empty(title: String, message: String, image: UIImage)
    }
    
    var displayState: EmptyDisplayState? {
        didSet {
            setNeedsLayout()
        }
    }
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator.color = .lightGray
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()
    
    private let emptyStateImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .lightGray
        return imageView
    }()
    
    private let emptyStateTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightSemibold)
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private let emptyStateMessageLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private let emptyStateStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 12
        stackView.distribution = .fill
        return stackView
    }()
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        emptyStateStackView.addArrangedSubview(emptyStateImageView)
        emptyStateStackView.addArrangedSubview(emptyStateTitleLabel)
        emptyStateStackView.addArrangedSubview(emptyStateMessageLabel)
        addSubview(activityIndicator)
        addSubview(emptyStateStackView)
        emptyStateStackView.translatesAutoresizingMaskIntoConstraints = false
        emptyStateStackView.widthAnchor.constraint(equalToConstant: 280).isActive = true
        emptyStateStackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        emptyStateStackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
    
    
    // MARK: - Layout and Display -
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        // Setup variables
        
        let topOffset = max(-contentOffset.y, contentInset.top)
        let contentInsets = UIEdgeInsets(top: topOffset, left: 32, bottom: 0, right: 32)
        let contentRect = UIEdgeInsetsInsetRect(bounds, contentInsets)
        let contentCenter = CGPoint(x: contentRect.midX, y: contentRect.midY)
        
        // Layout the activity indicator

        activityIndicator.sizeToFit()
        activityIndicator.center = contentCenter
        
        // Hide all if content exists
        
        guard let displayState = displayState, isEmpty == true else {
            activityIndicator.stopAnimating()
            emptyStateStackView.isHidden = true
            return
        }
        
        // Hide and show as needed
        
        switch displayState {
            
        case .loading:
            
            activityIndicator.startAnimating()
            emptyStateStackView.isHidden = true
            
        case .empty(let title, let message, let image):
            
            // Populate and position the empty state stack view
            
            activityIndicator.stopAnimating()
            emptyStateStackView.isHidden = false
            emptyStateTitleLabel.text = title
            emptyStateMessageLabel.text = message
            emptyStateImageView.image = image.withRenderingMode(.alwaysTemplate)
            var verticalOffset = contentOffset.y / 2
            
            if contentInset.top > -contentOffset.y {
                
                // Negate bouncing in the upwards direction for 
                // the empty stack view (it just feels a little weird
                // when you can bounce)
                
                verticalOffset = contentOffset.y + contentInset.top / 2
                
            }
            
            emptyStateStackView.transform = CGAffineTransform(translationX: 0, y: verticalOffset)
            
        }
        
    }
    
    private var isEmpty: Bool {
        
        for sectionIndex in 0 ..< numberOfSections {
            if numberOfItems(inSection: sectionIndex) > 0 { return false }
        }
        
        return true
        
    }
    
}
